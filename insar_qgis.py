# -*- coding: utf-8 -*-
"""
/***************************************************************************
 insar2qgis
                                 A QGIS plugin
 InSAR PS timeseries visualisation tool for QGIS3
                              -------------------
        begin                : 2020-03-06
        git sha              : $Format:%H$
        copyright            : (C) 2020 by Richard Czikhardt
        email                : czikhardt.richard@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from qgis.core import (QgsProject, QgsVectorLayer, Qgis, QgsMarkerSymbol, 
                       QgsRendererRange, QgsGraduatedSymbolRenderer)
from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication, Qt
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QFileDialog, QGraphicsScene
from qgis.PyQt import QtGui
# Initialize Qt resources from file resources.py
from .resources import *

# Import the code for the DockWidget
from .insar_qgis_dockwidget import insar2qgisDockWidget
import os.path
import numpy
import matplotlib.pyplot as plt
import matplotlib.dates as dates
formatter = dates.DateFormatter('%m-%y') 
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib
matplotlib.rcParams['figure.dpi'] = 80


class insar2qgis:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface

        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'insar2qgis_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&insar2qgis')
        self.toolbar = self.iface.addToolBar(u'insar2qgis')
        self.toolbar.setObjectName(u'insar2qgis')
        self.pluginIsActive = False
        self.dockwidget = None


    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('insar2qgis', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action


    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/insar_qgis/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Visualize InSAR data'),
            callback=self.run,
            parent=self.iface.mainWindow())

    #--------------------------------------------------------------------------

    def onClosePlugin(self):
        """Cleanup necessary items here when plugin dockwidget is closed"""

        #print "** CLOSING insar2qgis"

        # disconnects
        self.dockwidget.closingPlugin.disconnect(self.onClosePlugin)

        # remove this statement if dockwidget is to remain
        # for reuse if plugin is reopened
        # Commented next statement since it causes QGIS crashe
        # when closing the docked window:
        # self.dockwidget = None

        self.pluginIsActive = False


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""

        #print "** UNLOAD insar2qgis"

        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&insar2qgis'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def select_input_file(self):
        filename, _filter = QFileDialog.getOpenFileName(
                self.dockwidget, "Select input CSV ","", '*.shp')
        self.dockwidget.lineEdit.setText(filename)

    def loadCSV(self):
        # get filename from dialog box
        inCSV = self.dockwidget.lineEdit.text()
        inPath = os.path.split(inCSV)[0]
        baseName = os.path.split(inCSV)[1].split('.')[0]
        outSHP = inPath + os.sep + baseName + ".shp"

        # create vector layer:
        myVectorLayer = QgsVectorLayer(outSHP,'InSAR_'+baseName, 'ogr')
        targetField = 'VEL'
        groupList = []
        opacity = 1
   
        # colour list (RGB):
        colourList = [(165,0,0),
                      (197,0,0),
                      (229,0,0),
                      (255,11,0),
                      (255,78,0),
                      (255,146,0),
                      (255,213,0),
                      (239,255,16),
                      (194,255,61),
                      (149,255,106),
                      (105,255,150),
                      (60,255,195),
                      (15,255,240),
                      (0,214,255),
                      (0,151,255),
                      (0,87,255),
                      (0,24,255),
                      (0,10,224),
                      (0,5,187),
                      (0,0,150)]
        # convert to HEX:
        colourListHex = ['#%02x%02x%02x' % i for i in colourList]
        
        # range list (min,max)
        try:
            minVEL = float(self.dockwidget.lineEdit_3.text())
        except:
           minVEL = -5
        try:
            maxVEL = float(self.dockwidget.lineEdit_4.text())
        except:                                     
            maxVEL = 5        
        spacing = (maxVEL - minVEL)/20;
        lower = numpy.arange(minVEL,maxVEL,spacing).tolist()
        lowerTxt = [f'{x:.1f}' for x in lower]
        upper = numpy.arange(minVEL+spacing,maxVEL+spacing,spacing).tolist()
        upperTxt = [f'{x:.1f}' for x in upper]
        # change min/max value to higher
        lower[0] = -1000
        upper[-1] = 1000
        rangeList = [(lower[i], upper[i]) for i in range(0, len(lower))]
        # make legend tooltip:
        txtList = [i + ' to ' + j + ' [mm]' for i,j in zip(lowerTxt,upperTxt)]

        # prepare instances of symbol
        symbolList = [QgsMarkerSymbol() for i in range(len(colourList))]
        
        # create graduated symbol group
        groupList = []
        for colour, ran, symbol, txt in zip(colourListHex, rangeList, symbolList, txtList):
            symbol.setOpacity(opacity)
            symbol.setColor(QtGui.QColor(colour))
            symbol.symbolLayer(0).setStrokeStyle(0) # no overline
            ranTemp = QgsRendererRange(ran[0],ran[1],symbol,txt)
            groupList.append(ranTemp)
        
        myRenderer = QgsGraduatedSymbolRenderer('', groupList)
        myRenderer.setMode(QgsGraduatedSymbolRenderer.EqualInterval)
        myRenderer.setClassAttribute(targetField)
        
        myVectorLayer.setRenderer(myRenderer)
        psLayer = QgsProject.instance().addMapLayer(myVectorLayer)

        # add SHP as qgis layer:
        #psLayer = self.iface.addVectorLayer(outSHP, "InSAR", "ogr")
        if not psLayer:
            print("Layer failed to load!")
        self.iface.messageBar().pushMessage("Successfully loaded ", 
                             "input file = " + outSHP,
                             level=Qgis.Success, duration=3)
                     
    def select_PS(self):
        # turn on selection mode:
        self.iface.actionSelect().trigger()

    def plotTS(self):
        # get actove layer:
        layer = self.iface.activeLayer()
        if layer:
            count_selected = layer.selectedFeatureCount()
            if count_selected > 0:
                feature = layer.selectedFeatures()
                atrs = feature[0].attributes()
                vel = atrs[feature[0].fieldNameIndex('VEL')]
                coh = atrs[feature[0].fieldNameIndex('COHER')]
                std = atrs[feature[0].fieldNameIndex('STDEV')]
                temp = feature[0].fields().indexFromName('HEIGHT WRT')
                if temp == -1:
                    remotio_format = True
                    resH = atrs[feature[0].fieldNameIndex('HEIGHT_WRT')]
                    sig_vel = atrs[feature[0].fieldNameIndex('SIGMA_VEL')]
                    sig_h = atrs[feature[0].fieldNameIndex('SIGMA_HEIG')]
                else:
                    remotio_format = False
                    resH = atrs[temp]
                    sig_vel = atrs[feature[0].fieldNameIndex('SIGMA VEL')]
                    sig_h = atrs[feature[0].fieldNameIndex('SIGMA HEIG')]
                
                # add PS attributes to textBox:
                self.dockwidget.lineEdit_2.setText('ID: '+str(atrs[0]) + '   ' +
                                            'VEL: ' +f'{vel:.1f}' + ' ± ' + 
                                            f'{sig_vel:.1f}' + ' mm   ' +
                                            'ResH: ' +f'{resH:.1f}' + ' ± ' + 
                                            f'{sig_h:.1f}' + ' m   ' +
                                            'VarFact: ' +f'{coh:.1f}   ' +
                                            'STD: ' +f'{std:.1f} mm')
                # TODO: change params description according to remotio format
                
                # plot TS here:
                if remotio_format:
                    ts_idx = 17
                else:
                    ts_idx = 16
                displ = atrs[ts_idx:] # displacements
                # acquisition dates from layer attribute fields:
                acqDates = [acq.displayName() for acq in layer.fields()][ts_idx:]
                acqNum = [dates.datestr2num(acq) for acq in acqDates]
                
                # get min and max displ. from lineEdit:
                try: # if editBox empty
                    minD = float(self.dockwidget.lineEdit_6.text())
                    if self.formerId != atrs[0]:
                        minD = min(displ) - abs(max(displ)-min(displ))/20 # min - 5%
                        self.dockwidget.lineEdit_6.setText(f'{minD:.1f}')
                except: # use min from data and set into textbox
                    minD = min(displ) - abs(max(displ)-min(displ))/20 # min - 5%
                    self.dockwidget.lineEdit_6.setText(f'{minD:.1f}')                
                try: # if editBox empty
                    maxD = float(self.dockwidget.lineEdit_5.text())
                    if self.formerId != atrs[0]:  
                        maxD = max(displ) + abs(max(displ)-min(displ))/20 # max + 5%
                        self.dockwidget.lineEdit_5.setText(f'{maxD:.1f}')
                except: # use min from data and set into textbox
                    maxD = max(displ) + abs(max(displ)-min(displ))/20 # max + 5%
                    self.dockwidget.lineEdit_5.setText(f'{maxD:.1f}')
                self.formerId = atrs[0]
                
                # figure with size in inches (according to Qt)
                winSize = self.dockwidget.graphicsView.geometry().getRect()
                width = winSize[2]
                height = winSize[3]
                dpi = 80              
                plt.figure(figsize=(width/dpi*0.9,height/dpi*0.9),dpi=dpi)
                plt.plot(acqNum,displ,'b:^')
                #plt.setp(lines, 'linestyle', ':')
                plt.ylabel('displacement [mm]')
                plt.ylim(minD,maxD)
                plt.grid()
                # dates formatting:
                ax = plt.gca()
                #ax.xaxis.set_major_locator(dates.YearLocator())
                #ax.xaxis.set_major_formatter(dates.DateFormatter('%Y'))
                #ax.xaxis.set_minor_locator(dates.MonthLocator())
                start, end = ax.get_xlim()
                stepsize = (end+30-start)/10
                ax.xaxis.set_ticks(numpy.arange(start, end+30, stepsize))
                ax.xaxis.set_major_formatter(formatter)
                ax.xaxis.set_tick_params(rotation=30, labelsize=10)
                #plt.show()
                fig = plt.gcf()
                fig.tight_layout()
                # put into graphicsView using matplotlib backend
                self.scene = QGraphicsScene(self.dockwidget) 
                canvas = FigureCanvas(fig)
                self.scene.addWidget(canvas)
                self.dockwidget.graphicsView.setScene(self.scene)
                self.dockwidget.graphicsView.setSceneRect(0,0,
                                                          width*0.95,height*0.95)
                #self.dockwidget.graphicsView.fitInView()
                self.dockwidget.show()  
            else:
                self.iface.messageBar().pushCritical("Error",
                "Please select at least one feature from current layer")
        else:
            self.iface.messageBar().pushCritical("Error", "Please select a layer")
                

    #--------------------------------------------------------------------------

    def run(self):
        """Run method that loads and starts the plugin"""

        if not self.pluginIsActive:
            self.pluginIsActive = True

            #print "** STARTING insar2qgis"

            # dockwidget may not exist if:
            #    first run of plugin
            #    removed on close (see self.onClosePlugin method)
            if self.dockwidget == None:
                # Create the dockwidget (after translation) and keep reference
                self.dockwidget = insar2qgisDockWidget()
                # added functions:
                self.dockwidget.pushButton_4.clicked.connect(self.select_input_file)
                self.dockwidget.pushButton_3.clicked.connect(self.plotTS)
                self.dockwidget.pushButton_2.clicked.connect(self.select_PS)
                self.dockwidget.pushButton.clicked.connect(self.loadCSV)

            # connect to provide cleanup on closing of dockwidget
            self.dockwidget.closingPlugin.connect(self.onClosePlugin)

            # show the dockwidget
            # TODO: fix to allow choice of dock location
            self.iface.addDockWidget(Qt.LeftDockWidgetArea, self.dockwidget)
            self.dockwidget.show()
