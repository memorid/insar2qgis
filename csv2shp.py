###############################################################################
#  csv2shp.py
#
#  Project:     G-MaPIT
#  Purpose:     Routine to convert g-mapit or sarproz generated InSAR PS CSV 
#               standard into OGR SHP file ready for import into QGIS
#  Author:      Richard Czikhardt, czikhardt.richard@gmail.com
#  Last edit:   20.02.2020
#  Python dependencies: pandas, geopandas, fiona
#
###############################################################################
def csv2shp(inCSV,outSHP):

    import pandas as pd
    import geopandas
    from fiona.crs import from_epsg
    
    #inCSV = "/home/rc/sw/g-mapit/TS.csv"
    #outSHP = "/home/rc/CopernicusRUS/gmapit/Cunovo_DSC124.shp"
    
    # load CSV as pandas dataframe:
    data = pd.read_csv(inCSV)     
    # conver to geopandas dataframe:
    gdf = geopandas.GeoDataFrame(
        data, geometry=geopandas.points_from_xy(data.LON, data.LAT))    
    # fix CRS:
    gdf.crs = from_epsg(4326)    
    # save SHP:
    gdf.to_file(outSHP)
    
if __name__ == "__main__":
    import sys
    csv2shp(sys.argv[1],sys.argv[2])